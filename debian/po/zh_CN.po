# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Simplified Chinese translation for Debian Installer.
#
# Copyright (C) 2003-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Translated by Yijun Yuan (2004), Carlos Z.F. Liu (2004,2005,2006),
# Ming Hua (2005,2006,2007,2008), Xiyue Deng (2008), Kov Chai (2008),
# Kenlen Lai (2008), WCM (2008), Ren Xiaolei (2008).
#
#
# Translations from iso-codes:
#   Tobias Toedter <t.toedter@gmx.net>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#
#   Free Software Foundation, Inc., 2002, 2003, 2007, 2008.
#   Alastair McKinstry <mckinstry@computer.org>, 2001,2002.
#   Translations taken from KDE:
#   - Wang Jian <lark@linux.net.cn>, 2000.
#   - Carlos Z.F. Liu <carlosliu@users.sourceforge.net>, 2004 - 2006.
#   LI Daobing <lidaobing@gmail.com>, 2007, 2008, 2009, 2010.
#   YunQiang Su <wzssyqa@gmail.com>, 2011.
#
#   Mai Hao Hui <mhh@126.com>, 2001 (translations from galeon)
# YunQiang Su <wzssyqa@gmail.com>, 2010, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2011-08-11 11:13+0800\n"
"Last-Translator: YunQiang Su <wzssyqa@gmail.com>\n"
"Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: text
#. Description
#. :sl4:
#. Main menu item
#. Keep translations below 55 ccolumns (1 character=1 column
#. except for wide character languages such as Chinese/Japanese/Korean)
#. A bit of context for translators :
#. PReP stands for PowerPC Reference Platform, is an acronym, and should not be
#. translated. The PReP boot partition is a partition of type 0x41, which is
#. used by PReP boxes and IBM CHRP boxes to boot yaboot or the kernel from.
#: ../prep-installer.templates:1001
msgid "Install the kernel on a PReP boot partition"
msgstr "将内核安装到 PReP 引导分区"

#. Type: text
#. Description
#. :sl4:
#. Type: text
#. Description
#. :sl4:
#: ../prep-installer.templates:2001 ../prep-installer.templates:7001
msgid "Copying the kernel to the PReP boot partition"
msgstr "正在将内核复制到 PReP 引导分区"

#. Type: text
#. Description
#. :sl4:
#: ../prep-installer.templates:3001
msgid "Looking for PReP boot partitions"
msgstr "正在查找 PReP 引导分区"

#. Type: error
#. Description
#. :sl4:
#: ../prep-installer.templates:4001
msgid "No PReP boot partitions"
msgstr "没有 PReP 引导分区"

#. Type: error
#. Description
#. :sl4:
#: ../prep-installer.templates:4001
msgid ""
"No PReP boot partitions were found. You must create a PReP boot partition "
"within the first 8MB of your hard disk."
msgstr ""
"没有找到 PReP 引导分区。您必须在硬盘的前 8MB 空间中创建一个 PReP 引导分区。"

#. Type: text
#. Description
#. :sl4:
#: ../prep-installer.templates:5001
msgid "Looking for the root partition"
msgstr "正在查找根分区"

#. Type: error
#. Description
#. :sl4:
#: ../prep-installer.templates:6001
msgid "No root partition found"
msgstr "未找到根分区"

#. Type: error
#. Description
#. :sl4:
#: ../prep-installer.templates:6001
msgid ""
"No partition is mounted as your new root partition. You must mount a root "
"partition first."
msgstr "没有挂载新的根分区。您必须先挂载一个根分区。"

#. Type: note
#. Description
#. :sl4:
#: ../prep-installer.templates:8001
msgid "Successfully installed PReP"
msgstr "安装 PReP 成功"

#. Type: note
#. Description
#. :sl4:
#: ../prep-installer.templates:8001
msgid "The kernel was properly copied to the PReP boot partition."
msgstr "内核被成功复制到 PReP 引导分区。"

#. Type: note
#. Description
#. :sl4:
#: ../prep-installer.templates:8001
msgid "The new system is now ready to boot."
msgstr "可以启动新系统了。"
